const accordionItems = document.querySelectorAll('.accordion-item');

accordionItems.forEach(item => {
    const header = item.querySelector('.accordion-header');
    const content = item.querySelector('.accordion-content');
    const closeButton = item.querySelector('.close');

    header.addEventListener('click', () => {
        accordionItems.forEach(otherItem => {
            const otherContent = otherItem.querySelector('.accordion-content');
            if (otherItem !== item) {
                otherContent.style.display = 'none';
            }
        });

        content.style.display = content.style.display === 'block' ? 'none' : 'block';
    });

    closeButton.addEventListener('click', () => {
        accordionItems.forEach(otherItem => {
            const otherContent = otherItem.querySelector('.accordion-content');
            if (otherItem !== item) {
                otherContent.style.display = 'none';
            }
        });

        content.style.display = content.style.display === 'block' ? 'none' : 'block';
    });
});


// init swipper
const swiperInstance = new Swiper('.mySwiper', {
    slidesPerView: 6,
    spaceBetween: 30,  
    autoplay: {
        delay: 2000,
    },
});

// init swiper article

const swiperArticle = new Swiper('.articleSwiper', {
    slidesPerView: 3,
    spaceBetween: 130,
    centeredSlides: false,
})