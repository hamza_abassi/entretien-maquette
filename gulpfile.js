import gulp from 'gulp';
import cssmin from 'gulp-cssmin';
import uglify from 'gulp-uglify';
import connect from 'gulp-connect';
import imagemin from 'gulp-imagemin';
import browserSync from 'browser-sync';

const port = 7000;
const paths = {
    css: 'css/**/*.css',
    js: 'js/**/*.js',
    img: 'img/**/*.{png,jpg,gif,jpeg,svg}',
    html: 'index.html',
};

// Tâche pour minifier les fichiers CSS
gulp.task('minify-css', () => {
    return gulp.src(paths.css)
        .pipe(cssmin())
        .pipe(gulp.dest('dist/css'))
        .pipe(connect.reload());
});

// Tâche pour minifier les fichiers JS
gulp.task('minify-js', () => {
    return gulp.src(paths.js)
        .pipe(uglify())
        .pipe(gulp.dest('dist/js'))
        .pipe(connect.reload());
});

// Tâche pour optimiser les images en utilisant gulp-imagemin
gulp.task('images', () => {
	return gulp.src(paths.img)
		.pipe(imagemin())
		.pipe(gulp.dest('dist/img'));
});

// Tâche pour copier le fichier index.html
gulp.task('copy-html', () => {
    return gulp.src(paths.html)
        .pipe(gulp.dest('dist'))
        .pipe(connect.reload());
});

// Tâche pour lancer un serveur en utilisant BrowserSync
gulp.task('serve', () => {
    browserSync.init({
        server: {
            baseDir: './dist',
        },
        port: port,
    });

    // Surveillez les fichiers CSS, JS, HTML, et images pour le rechargement en direct
    gulp.watch(paths.css, gulp.series('minify-css')).on('change', browserSync.reload);
    gulp.watch(paths.js, gulp.series('minify-js')).on('change', browserSync.reload);
    gulp.watch(paths.html, gulp.series('copy-html')).on('change', browserSync.reload);
    gulp.watch(paths.img, gulp.series('images')).on('change', browserSync.reload);
});

// Tâche de build complète
gulp.task('build', gulp.series('minify-css', 'minify-js', 'images', 'copy-html'));

// Tâche de déploiement
gulp.task('deploy', () => {
    return gulp.src(['dist/**/*'])
        .pipe(gulp.dest('public_html'));
});

// Tâche par défaut (exécute les tâches minify-css, minify-js, copy-html, images, serve)
gulp.task('default', gulp.parallel('minify-css', 'minify-js', 'copy-html', 'images', 'serve'));
